import axios from 'axios'

axios.defaults.baseURL = 'http://mentor/api/v1/'

axios.defaults.headers.Authorization = `Bearer ${localStorage.getItem(
  'mentor_auth_token'
)}`

export const fetchLessons = async (data = {}) => {
  return (await axios.get('/lessons', { params: data })).data
}

export const fetchUsersLessons = async (data = {}) => {
  return (await axios.get('/lessons/forAuth', { params: data })).data
}

export const fetchLesson = async (id = null) => {
  return (await axios.get(`/lessons/${id}`)).data
}

export const createLesson = (data) => {
  return axios.post('/lessons', data)
}

export const updateLesson = (id, data) => {
  return axios.put(`/lessons/${id}`, data)
}

export const deleteUsersLessons = (params) => {
  return axios.delete('lessons', { params })
}

export const fetchUserSlots = () => {
  return axios.get('/slots')
}

export const createUserSlots = (data) => {
  return axios.post('/slots', data)
}

export const deleteUserSlot = (id) => {
  return axios.delete(`/slots/${id}`)
}

export const fetchCategories= async (data = {}) => {
  return (await axios.get('/categories', { params: data })).data
}

export const fetchSlots= async (id = null, data = {}) => {
  return (await axios.get(`/slots/${id}`, { params: data })).data
}

export const loginRequest = (auth) => {
  return axios.post('/login', [], { auth })
}

export const registerRequest = (data) => {
  return axios.post('/users',  data)
}

export const loginFacebookRequest = (data) => {
  return axios.post('/facebook-login', data)
}

export const fetchUser = () => {
  return axios.get('/users')
}

export const changePasswordRequest = (data) => {
  return axios.post('/reset-password', data)
}

export const loginGoogleRequest = (data) => {
  return axios.post('/google-login', data)
}

export const patchUser = (user) => {
  return axios.post('/update-user', user)
}

export const postUserAvatar = (image) => {
  return axios.post('/update-user-avatar', image)
}

export const postImage = (image) => {
  return axios.post('/upload-img', image)
}

