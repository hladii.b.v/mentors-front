import {
  CLEAR_USER,
  SET_LOGGING,
  SET_USER,
  SET_USER_AVATAR,
} from '../actions/actionTypes'

const initialState = {
  id: null,
  role: '',
  name: '',
  last_name: '',
  email: '',
  image: '',
  phone: '',
  time_zone: ''
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER:
      return { ...state, ...action.user };
    case CLEAR_USER:
      return {
        id: null,
        role: '',
        name: '',
        last_name: '',
        email: '',
        image: '',
        phone: '',
        time_zone: ''
      };
    case SET_LOGGING:
      return {
        ...state,
        isLogging: action.isLogging,
      };
    case SET_USER_AVATAR:
      return {
        ...state,
        avatar: action.avatar,
      };
    default:
      return state;
  }
}
