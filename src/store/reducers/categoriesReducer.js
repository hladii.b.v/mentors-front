import { SET_CATEGORIES } from '../actions/actionTypes'

const initialState = {
 all: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CATEGORIES:
      return { ...state, all: action.categories }
    default:
      return state
  }
}
