import {
  ADD_USER_LESSON,
  DELETE_USER_LESSONS,
  REPLACE_USER_LESSON,
  SET_USER_LESSONS,
} from '../actions/actionTypes'

const initialState = {
  all: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_LESSONS:
      return {
        ...state,
        all: action.lessons,
      }
    case ADD_USER_LESSON:
      return {
        ...state,
        all: [...state.all, ...action.lesson],
      }
    case DELETE_USER_LESSONS:
      return {
        ...state,
        all: state.all.filter((lesson) =>
          !action.lessons.some(({ id }) => id === lesson.id)
        ),
      }
    case REPLACE_USER_LESSON:
      return {
        ...state,
        all: state.all.map((lesson) =>
          lesson.id === action.lesson.id ? action.lesson : lesson
        ),
      }
    default:
      return state
  }
}
