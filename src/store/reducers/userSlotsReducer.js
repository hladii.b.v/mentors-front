import {
  ADD_USER_SLOT,
  DELETE_USER_SLOT,
  REPLACE_USER_SLOT,
  SET_USER_SLOTS,
} from '../actions/actionTypes'

const initialState = {
  all: [],
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_SLOTS:
      return {
        ...state,
        all: action.slots,
      }
    case ADD_USER_SLOT:
      return {
        ...state,
        all: [...state.all, ...action.slots],
      }
    case DELETE_USER_SLOT:
      return {
        ...state,
        all: state.all.filter((slot) => slot.id.toString() !== action.id.toString()),
      }
    case REPLACE_USER_SLOT:
      return {
        ...state,
        all: state.all.map((site) =>
          site.id === action.slot.id ? action.slot : site
        ),
      }
    default:
      return state
  }
}
