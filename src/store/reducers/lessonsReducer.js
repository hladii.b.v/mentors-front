import { SET_LESSONS } from '../actions/actionTypes'

const initialState = {
 all: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LESSONS:
      return { ...state, all: action.lessons }
    default:
      return state
  }
}
