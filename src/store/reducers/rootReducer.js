import { combineReducers } from 'redux'

import lessonsReducer from './lessonsReducer'
import userReducer from './userReducer'
import userSlotsReducer from './userSlotsReducer'
import categoriesReducer from './categoriesReducer'
import userLessonsReducer from './userLessonsReducer'

export default combineReducers({
  lessons: lessonsReducer,
  categories: categoriesReducer,
  user: userReducer,
  userSlots: userSlotsReducer,
  userLessons: userLessonsReducer
})
