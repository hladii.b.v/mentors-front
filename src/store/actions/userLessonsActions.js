import { createLesson, deleteUsersLessons, fetchUsersLessons, updateLesson } from '../api/api'
import { ADD_USER_LESSON, DELETE_USER_LESSONS, REPLACE_USER_LESSON, SET_USER_LESSONS } from './actionTypes'

export const createLessonAction = (data) => {
  return async (dispatch) => {
    return createLesson(data)
      .then((res) => {
        const { gig: lesson } = res.data
        dispatch({ type: ADD_USER_LESSON, lesson })

        return lesson
      })
      .catch((e) => {
        return e
      })
  }
}

export const updateLessonAction = (id, data) => {
  return async (dispatch) => {
    return updateLesson(id, data)
      .then((response) => {
        const { gig: lesson } = response.data
        console.log(lesson)
        if (lesson) {
          dispatch({ type: REPLACE_USER_LESSON, lesson })
          return true
        } else {
          return false
        }
      })
      .catch(() => false)
  }
}

export const getLessonsAction = (data) => {
  return async (dispatch) => {
    return fetchUsersLessons(data)
      .then((response) => {
        const { gigs, success } = response

        if (success) {
          dispatch({ type: SET_USER_LESSONS, lessons: gigs })
          return true
        } else {
          return false
        }
      })
      .catch(() => false)
  }
}

export const deleteLessonsAction = (data) => {
  return async (dispatch) => {
    return deleteUsersLessons(data)
      .then((response) => {
        const { gigs, success } = response.data

        if (success) {
          dispatch({ type: DELETE_USER_LESSONS, lessons: gigs })
          return true
        } else {
          return false
        }
      })
      .catch(() => false)
  }
}