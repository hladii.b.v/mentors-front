import {
  fetchLessons
} from '../api/api'
import { SET_LESSONS } from './actionTypes'

export const getLessonsAction = (data) => {
  return async (dispatch) => {
    return fetchLessons(data)
      .then((response) => {
        const { gigs, success } = response

        if (success) {
          dispatch({ type: SET_LESSONS, lessons: gigs })
          return true
        } else {
          return false
        }
      })
      .catch(() => false)
  }
}