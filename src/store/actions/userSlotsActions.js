import { createUserSlots, deleteUserSlot, fetchUserSlots } from '../api/api'
import { ADD_USER_SLOT, DELETE_USER_SLOT, SET_USER_SLOTS } from './actionTypes'

export const getUserSlotsAction = () => {
  return async (dispatch) => {
    const response = await fetchUserSlots();
    if (response.status === 200) {
      dispatch({
        type: SET_USER_SLOTS,
        slots: response.data.slots,
      })
    }

    if (response.status === 401 && localStorage.getItem('mentor_auth_token')) {
      getUserSlotsAction()
    }
  }
}

export const createSlotAction = (data) => {
  return async (dispatch) => {
    return createUserSlots(data)
      .then((res) => {
        const { slots } = res.data
        dispatch({ type: ADD_USER_SLOT, slots })

        return slots
      })
      .catch((e) => {
        return e
      })
  }
}

export const deleteSlotAction = (id) => {
  return (dispatch) => {
    deleteUserSlot(id).then(() => {
      dispatch({
        type: DELETE_USER_SLOT,
        id,
      })
    })
  }
}
