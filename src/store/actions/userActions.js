import {
  changePasswordRequest,
  fetchUser,
  loginFacebookRequest,
  loginGoogleRequest,
  loginRequest,
  patchUser,
  postImage,
  postUserAvatar,
  registerRequest,
} from '../api/api'
import {CLEAR_USER, SET_LOGGING, SET_USER, SET_USER_AVATAR} from './actionTypes'

import axios from 'axios'

export const loginAction = (data) => {
  return async (dispatch) => {
    return loginRequest(data)
      .then((response) => {
        const { user, success } = response.data

        if (success) {
          const  token = user.api_token

          localStorage.setItem('mentor_auth_token', token)
          axios.defaults.headers.common.Authorization = `Bearer ${token}`
          dispatch({ type: SET_USER, user })

          return true
        } else {
          return false
        }
      })
      .catch(() => false)
  }
}

export const facebookLoginAction = (data) => {
  return async (dispatch) => {
    loginFacebookRequest(data).then((res) => {
      const { user, success } = res.data

      if (success) {
        const  token = user.api_token

        localStorage.setItem('mentor_auth_token', token)
        axios.defaults.headers.common.Authorization = `Bearer ${token}`
        dispatch({ type: SET_USER, user })

        return true
      } else {
        return false
      }
    })
  }
}

export const googleLoginAction = (data) => {
  return async (dispatch) => {
    loginGoogleRequest(data).then((res) => {
      const { user, success } = res.data

      if (success) {
        const  token = user.api_token

        localStorage.setItem('mentor_auth_token', token)
        axios.defaults.headers.common.Authorization = `Bearer ${token}`
        dispatch({ type: SET_USER, user })

        return true
      } else {
        return false
      }
    })
  }
}

export const registerAction = (data) => {
  return async (dispatch) => {
    return registerRequest({ ...data })
      .then((res) => {
        const { user, success } = res.data

        if (success) {
          const  token = user.api_token

          localStorage.setItem('mentor_auth_token', token)
          axios.defaults.headers.common.Authorization = `Bearer ${token}`
          dispatch({ type: SET_USER, user })

          return true
        } else {
          return false
        }
      })
      .catch(() => false)
  }
}

export const getUserAction = () => {
  return async (dispatch) => {
    return await fetchUser()
      .then((res) => {
        const {user} = res.data
        dispatch({ type: SET_USER, user })
      })
      .catch(() => {
        dispatch({ type: SET_LOGGING })
      })
  }
}

export const uploadImageAction = (imageFormData) => {
  return async (dispatch) => {
    const response = await postImage(imageFormData)
   
    // if (response.status === 200) { 
      
    //   dispatch({ type: ADD_USER_IMAGES, images: response.data.url, key: type })
    // }
    return response?.data
  }
}

export const uploadAvatarAction = (imageFormData) => {
  return async (dispatch) => {
    postUserAvatar(imageFormData).then((res) => {
      dispatch({
        type: SET_USER_AVATAR,
        avatar: res.data.url,
      })
    })
  }
}

export const editUserAction = (user) => {
  return async (dispatch) => {
    return patchUser(user)
      .then((response) => {})
      .catch(() => false)
  }
}

export const logoutUserAction = () => {
  return async (dispatch) => {
    dispatch({type: CLEAR_USER})
    localStorage.removeItem('mentor_auth_token')
  }
}

export const changePasswordAction = (password, code) => {
  return async (dispatch) => {
    return changePasswordRequest({ password, code }).then((res) => {
      dispatch({ type: SET_USER, user: res.data.user })
      return true
    })
  }
}