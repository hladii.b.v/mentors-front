import {
  fetchCategories
} from '../api/api'
import { SET_CATEGORIES } from './actionTypes'

export const getCategoriesAction = (data) => {
  return async (dispatch) => {
    return fetchCategories(data)
      .then((response) => {
        const { categories, success } = response

        if (success) {
          dispatch({ type: SET_CATEGORIES, categories })
          return true
        } else {
          return false
        }
      })
      .catch(() => false)
  }
}