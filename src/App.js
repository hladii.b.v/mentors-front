import React, { Suspense, useEffect } from 'react'

import { connect } from 'react-redux'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Redirect } from 'react-router'

import Home from './Components/Pages/Home/Home'
import Lessons from './Components/Pages/Lessons/Lessons'
import { Lesson } from './Components/Pages/Lesson/Lesson'
import Login from './Components/Pages/Login/Login'
import Register from './Components/Pages/Register/Register'
import Dashboard from './Components/Pages/Dashboard/Dashboard'
import { getUserAction } from './store/actions/userActions'
import CreateLesson from './Components/Pages/CreateLesson/CreateLesson'
import UserLessons from './Components/Pages/UserLessons/UserLessons'

const PrivateRoute = ({ component: Component, state = {}, ...rest }) => (
  <Route {...rest}>
    {
      localStorage.getItem('mentor_auth_token') ? (
        <Component />
      ) : (
        <Redirect to={{ pathname: '/login', state }} />
      )}
  </Route>
)

function App ({ getUser }) {
  useEffect(() => {
    (async () => {
      await getUser()
    })()
  }, [])

   return <Router>
      <div className='container'>
        <Suspense fallback={<div className='fallback' />}>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/lessons' component={Lessons} />
            <Route exact path='/categories/:id' component={Lessons} />
            <Route exact path='/lessons/:id' component={Lesson} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/register/:mode' component={Register} />
            <PrivateRoute exact path='/dashboard' component={Dashboard} />
            <PrivateRoute exact path='/dashboard/lesson' component={CreateLesson} />
            <PrivateRoute exact path='/dashboard/lesson/:id' component={CreateLesson} />
            <PrivateRoute exact path='/dashboard/lessons' component={UserLessons} />
          </Switch>
        </Suspense>
      </div>
    </Router>
}

const mapStateToProps = (state) => ({
  user: state.user
})

const mapDispatchToProps = (dispatch) => ({
  getUser: () => dispatch(getUserAction())
})

export default connect(mapStateToProps, mapDispatchToProps)(App)