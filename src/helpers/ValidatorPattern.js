export class ValidatorPattern {
  constructor () {
    this.rules = this.getInitialRules()
  }

  getInitialRules () {
    /**
     * Rule example:
     * {
     *   property: 'name',
     *   asserts: [
     *     {
     *       message: 'Name can not be empty',
     *       callback: value => !!value
     *     },
     *     {
     *       message: 'Name can not be the same as nickname',
     *       context: ['nickname'],
     *       callback: (name, nickname) => name !== nickname
     *     }
     *   ]
     * }
     */
    return []
  }

  validate (validatable) {
    const errors = {}

    for (const rule of this.rules) {
      for (const assert of rule.asserts) {
        if (
          !assert.callback.apply(
            null,
            [validatable[rule.property]].concat((assert.context || []).map(property => validatable[property]))
          )
        ) {
          if (!errors.hasOwnProperty(rule.property)) {
            errors[rule.property] = []
          }

          errors[rule.property] = errors[rule.property].concat([assert.message.replace(/:property/, rule.property)])
        }
      }
    }

    return errors
  }

  static notEmptyAssert (message = ':property cannot be empty') {
    return {
      callback: value => !!value || value === 0,
      message
    }
  }
}
