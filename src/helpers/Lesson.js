export const Langs = {
  ua: 'Українська',
  ru: 'Російська',
  en: 'Англійська'
}

export const Levels = {
  beginner: 'Початківець',
  middle: 'Достатній',
  expert: 'Експерт'
}

export const Durations = [
  30,
  45,
  60
]