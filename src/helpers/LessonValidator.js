import { ValidatorPattern } from './ValidatorPattern'

export class LessonValidator extends ValidatorPattern {
  getInitialRules () {
    return [
      {
        property: 'title',
        asserts: [
          ValidatorPattern.notEmptyAssert('Ім`я уроку обовязкове')
        ]
      },
      {
        property: 'price',
        asserts: [
          ValidatorPattern.notEmptyAssert('Ціна уроку обовязкова')
        ]
      },
      {
        property: 'duration',
        asserts: [
          ValidatorPattern.notEmptyAssert('Тривалість уроку обовязкова')
        ]
      },
      {
        property: 'category_id',
        asserts: [
          ValidatorPattern.notEmptyAssert('Категорія уроку обовязкова')
        ]
      }
    ]
  }
}
