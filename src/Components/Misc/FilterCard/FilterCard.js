import React from 'react'

import s from './FilterCard.module.css'

export const FilterCard = ({ categories, className, fetchCategory }) => {
  return (
    <div className={s.container}>
      <div onClick={() => fetchCategory('null')} className={`${s.item} ${className}`}>Всі категорії</div>
      {
        categories.map((category) =>
          <div key={category.id} onClick={() => fetchCategory(category.id)} className={`${s.item} ${className}`}>{category.title}</div>
        )
      }
    </div>
  )
}
