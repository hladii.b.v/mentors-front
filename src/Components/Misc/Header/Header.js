import React, { useState, useEffect } from 'react'

import { Link } from 'react-router-dom'
import classnames from 'classnames'
import { useHistory } from 'react-router'
import { connect } from 'react-redux'

import s from './Header.module.css'

import { Button } from '../Button/Button'
import { logoutUserAction } from '../../../store/actions/userActions'

import { BsChevronDown } from 'react-icons/bs'
import {FiPlusCircle} from "react-icons/fi";

const Header = ({ logout, user, showImg = false }) => {
  const [ visible, setVisible ] = useState(false)
  const [ isUser, setIsUser] = useState(false)

  const history = useHistory()

  useEffect(() => {
    if (localStorage.getItem('mentor_auth_token')) {
      setIsUser(true)
    }
  }, [])

  return (
    <>
      <div>
        <div
          className={classnames(showImg && s.header__img, s.header__container, s.desktop__header)}
        >
          <div className={s.container}>
            <div className={s.left__container}>
              <Link to='/'>
                <img
                  src={require('../../../assets/logo.png')}
                  className={s.logo}
                  alt='loading'
                />
              </Link>
            </div>
            <div className={s.right__container}>
              {
                !isUser && <>
                  <Button
                    onClick={() => history.push('login')}
                    title='Вхід'
                    className={s.login__button}
                    size='lg'
                  />
                  <Button
                    onClick={() => history.push('register/student')}
                    title='Реєстрація'
                    className={s.register__button}
                    size='lg'
                  />
                </>
              }
              {
                isUser && <div className={s.header__widget}>
                  <div className={s.user__menu}>
                    <div className={classnames(showImg && s.burger__icon, s.user__name)} onClick={() => setVisible(!visible)}>
                      <img className={s.img} src={user.image || require('../../../assets/dashboard-avatar.jpg')} alt='avatar' />
                      <span>
                        {user.name}
                      </span>
                      <BsChevronDown style={{ marginLeft: '6px' }} />
                    </div>
                    {
                      visible && <div className={s.user__menu__list}>
                        <Link className={s.user__menu__item} to='/dashboard'>Панель управління</Link>
                        <Link className={s.user__menu__item} onClick={logout}>Вихід</Link>
                      </div>
                    }
                  </div>
                  {
                    user.role === 'mentor' && <div className={s.create__button}>
                      <Button className={s.button__border} onClick={() => history.push('/dashboard/lesson')}><FiPlusCircle/>&nbsp; Створити урок</Button>
                    </div>
                  }
                </div>
              }
            </div>
          </div>
          {
            showImg && <div className={s.home__header__inner}>
              <div className={s.home__header__main__content}>
                <Button
                  onClick={() => history.push('register/mentor')}
                  title='Стати вчителем'
                  className={s.header__button}
                  size='lg'
                />
                <Button
                  onClick={() => history.push('lessons')}
                  title='Знайти вчителя'
                  className={s.header__button}
                  size='lg'
                />
              </div>
            </div>
          }
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  user: state.user
})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logoutUserAction())
})

export default connect(mapStateToProps, mapDispatchToProps)(Header)
