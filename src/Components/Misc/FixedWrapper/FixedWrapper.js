import React from 'react'

import s from './FixedWrapper.module.css'

export const FixedWrapper = ({ children, className }) => {
  return (
    <div className={s.wrapper}>
      <div className={`${s.container} ${className}`}>{children}</div>
    </div>
  )
}
