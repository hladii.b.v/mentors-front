import React from 'react'

import { useHistory } from 'react-router'

import s from './TemplateCard.module.css'

import { Button } from '../Button/Button'
import { CardWrapper } from '../CardWrapper/CardWrapper'

export const TemplateCard = ({ lesson, containerClass }) => {
  const { title, id } = lesson
  const history = useHistory()
  const redirectToSingle = () => history.push(`/lessons/${id}`)

  return (
    <CardWrapper
      className={`${s.wrapper} ${s.card__wrapper} ${containerClass}`}
    >
      <div className={s.container}>
        <div className={s.price__container}>
          <div className={s.price__item}>{lesson.price}&#8372;</div>
        </div>
        <img
          className={s.img}
          src={`http://mentor/image/gig/${id}`}
          alt='loading'
        />
        <div className={s.content}>
          {
            lesson.category_id && <div className={s.content__info}>
              <h4 className={s.content__title}>{lesson.category.title}</h4>
            </div>
          }
          <div className={s.buttons__container}>
            <div>
              <Button
                title={title}
                className={s.button}
                size='lg'
                onClick={redirectToSingle}
              />
            </div>
          </div>
        </div>
      </div>
    </CardWrapper>
  )
}

