import React, { useEffect, useState } from 'react'

import DatePicker from 'react-datepicker'
import { format } from 'date-fns'
import { Dropdown, Field, Menu, Item, Select } from '@zendeskgarden/react-dropdowns'

import s from './BookingCard.module.css'

import { fetchSlots } from '../../../store/api/api'

import { BiCheckCircle, BiCalendarCheck } from 'react-icons/bi'
import { GiCancel } from 'react-icons/gi'

export const BookingCard = ({ lesson }) => {
  const { id, verified } = lesson
  const [ startDate, setStartDate ] = useState(new Date())
  const [ slots, setSlots ] = useState([])
  const [ selectedItem, setSelectedItem ] = useState(null)
  const [ loading, setLoading ] = useState(true)

  useEffect(() => {
    (async () => {
      setLoading(true)
      const { slots } = await fetchSlots(id,{ date: format(startDate, 'yyyy-MM-dd') })

      setSlots(slots)
      setLoading(false)
    })()
  }, [startDate])

  return (
    <div
      className={`${s.wrapper}`}
    >
      <div className={`${s.verified__blade} ${verified ? s.verified : s.not__verified}`}>
        {
          verified ? <BiCheckCircle /> : <GiCancel />
        }
        <span style={{ marginLeft: '12px' }}>Verified Listing</span>
      </div>
      <div>
        <h3 className={s.booking__header}><BiCalendarCheck /> Бронювання</h3>
      </div>
      <div className={s.calendar__wrapper}>
        <DatePicker
          selected={startDate}
          onChange={(date) => setStartDate(date)}
          minDate={new Date()}
        />
        <Dropdown
          selectedItem={selectedItem}
          onSelect={setSelectedItem}
        >
          <Field>
            <Select disabled={loading}>{selectedItem ? slots[selectedItem] : `Слоти на ${format(startDate, 'yyyy-MM-dd')}`}</Select>
          </Field>
          <Menu>
            {
              slots.map((option, index) => (
                <Item key={index} value={index}>
                  {option}
                </Item>
              ))
            }
            {
              slots.length === 0 && <Item disabled>Немає вільних слотів</Item>
            }
          </Menu>
        </Dropdown>
      </div>
    </div>
  )
}
