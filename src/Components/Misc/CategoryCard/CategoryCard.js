import React from 'react'

import { useHistory } from 'react-router'

import s from './CategoryCard.module.css'

import { Button } from '../Button/Button'
import { CardWrapper } from '../CardWrapper/CardWrapper'

export const CategoryCard = ({category, containerClass}) => {
  const { title, description, id } = category
  const history = useHistory()
  const redirectToSingle = () => history.push(`/categories/${id}`)

  return (
    <Button
      isLink
      onClick={redirectToSingle}
      className={s.container}
      children={
        <CardWrapper
          className={`${s.wrapper} ${s.card__wrapper} ${containerClass}`}
        >
          <div className={s.img__container}>
            <img src={require('../../../assets/icon-home.png')} alt={category.title}/>
          </div>
          <h4>{title}</h4>
          <span className='category-box-counter'>{description}</span>
        </CardWrapper>
      }
    />
  )
}

