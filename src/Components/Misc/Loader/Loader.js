import React from 'react'

import Loader from 'react-loader-spinner'

import s from './Loader.module.css'

import { FixedWrapper } from '../FixedWrapper/FixedWrapper'

export const PageLoader = () => {
  return (
    <FixedWrapper className={s.loader__container}>
      <Loader type='Oval' color='#404040' height={100} width={100} />
    </FixedWrapper>
  )
}

export const Loading = () => {
  return (
    <FixedWrapper className={s.loader__container}>
      <Loader type='TailSpin' color='#404040' />
    </FixedWrapper>
  )
}
