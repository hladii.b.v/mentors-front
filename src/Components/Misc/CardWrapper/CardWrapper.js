import React from 'react'

import s from './CardWrapper.module.css'

export const CardWrapper = ({ children, className }) => {
  return (
    <div className={`${s.container} ${s.wrapper} ${className}`}>{children}</div>
  )
}
