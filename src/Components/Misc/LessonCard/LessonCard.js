import React from 'react'

import { Tag } from '@zendeskgarden/react-tags'

import 'react-tabs/style/react-tabs.css'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import 'react-datepicker/dist/react-datepicker.css'
import s from './LessonCard.module.css'

import { CardWrapper } from '../CardWrapper/CardWrapper'
import { FixedWrapper } from '../FixedWrapper/FixedWrapper'
import { BookingCard } from '../BookingCard/BookingCard'
import { Langs, Levels } from '../../../helpers/Lesson'

import { GiPriceTag } from 'react-icons/gi'
import { AiTwotonePhone, AiTwotoneMail, AiOutlineCheckSquare } from 'react-icons/ai'

export const LessonCard = ({ lesson, containerClass }) => {
  const { id, title, category, price, user, benefits = [], duration, languages = [], description, accomplishments, level, exprience_years } = lesson

  return (
    <CardWrapper
      className={`${s.wrapper} ${containerClass}`}
    >
      <div className={s.section}>
        <FixedWrapper className={s.cards__container}>
          <div className={s.lesson__wrap}>
            <div className={s.titlebar__wrap}>
              <img
                className={s.img}
                src={`http://mentor/image/gig/${id}`}
                alt='loading'
              />
              <div className={s.title__col}>
                <h2>{title}</h2>
                <div className={s.category__wrap}>
                  <div className={s.category__col}>
                    Email:&nbsp;<AiTwotoneMail />{user.email}
                  </div>
                  {
                    user.phone && <div className={s.category__col}>
                      Тел:&nbsp;<AiTwotonePhone />{user.phone}
                    </div>
                    }
                  {
                    category && <div className={s.category__col}>
                      Категорія: <Tag hue='green'>
                        <span>{category.title}</span>
                      </Tag>
                    </div>
                  }
                  <div className={s.category__col}>
                    Ціна: <Tag hue='green'>
                      <span><GiPriceTag/> {price} грн за {duration} хв</span>
                    </Tag>
                  </div>
                </div>
              </div>
            </div>
            <div className={s.listing__section}>
              <div className={s.section__wrap}>
                {
                  languages && <div className={s.section__col}>
                    <h3>Мови викладання:</h3>
                    {
                      languages.map((lang, index) =>
                        <span key={index}>&nbsp;{Langs[lang]}</span>
                      )
                    }
                  </div>
                }
                {
                  benefits && <div className={s.section__col}>
                    <h3>Переваги:</h3>
                    {
                      benefits.map((benefit, index) =>
                        <span key={index} className={s.tags__container}>
                          <Tag hue='green' key={index}><AiOutlineCheckSquare />&nbsp;{benefit}</Tag>
                        </span>
                      )
                    }
                  </div>
                }
              </div>
              <div className={s.section__wrap}>
                {
                  level && <div className={s.section__col}>
                    <h3>Рівень:</h3>
                    &nbsp;{Levels[level]}
                  </div>
                }
                {
                  exprience_years && <div className={s.section__col}>
                    <h3>Досвід:</h3>
                    &nbsp;{exprience_years} роки
                  </div>
                }
              </div>
            </div>
            <div className={s.listing__section}>
              {
                description && <div className={s.section__wrap}>
                  {description}
                </div>
              }
              {
                accomplishments && <div className={s.section__wrap}>
                  {accomplishments}
                </div>
              }
            </div>
          </div>
          <BookingCard lesson={lesson}/>
        </FixedWrapper>
      </div>
    </CardWrapper>
  )
}
