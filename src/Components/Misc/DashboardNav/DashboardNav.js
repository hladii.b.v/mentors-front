import React from 'react'

import { connect } from 'react-redux'
import { Link, NavLink } from 'react-router-dom'

import s from './DashboardNav.module.css'

import { logoutUserAction } from '../../../store/actions/userActions'

import { FiSettings, FiLogOut, FiMessageSquare, FiLayers, FiStar, FiUser, FiBarChart } from 'react-icons/fi'
import { BiCalendarCheck } from 'react-icons/bi'

const DashboardNav = ({ className, logout }) => {
  return <div className={`${s.container} ${className}`}>
    <div className={s.dashboard__inner}>
      <div className={s.menu__items}>
        <span className={s.menu__items__title}>Основне</span>
        <ul className={s.menu__list}>
          <li className={s.menu__list__item}>
            <NavLink
              to='/dashboard'
              className={s.menu__list__link}
              activeClassName={s.is__active}
            >
              <FiSettings/> Панель управління
            </NavLink>
          </li>
          <li className={s.menu__list__item}>
            <NavLink
              to='/booking'
              className={s.menu__list__link}
              activeClassName={s.is__active}
            >
              <BiCalendarCheck/> Бронювання
            </NavLink>
          </li>
          <li className={s.menu__list__item}>
            <NavLink
              to='/booking'
              className={s.menu__list__link}
              activeClassName={s.is__active}
            >
              <FiBarChart/> Статистика
            </NavLink>
          </li>
          <li className={s.menu__list__item}>
            <NavLink to='/booking' className={s.menu__list__link} activeClassName={s.is__active}><FiMessageSquare/> Повідомлення</NavLink>
          </li>
        </ul>
      </div>
      <div className={s.menu__items}>
        <span className={s.menu__items__title}>Уроки</span>
        <ul className={s.menu__list}>
          <li className={s.menu__list__item}>
            <NavLink to='/dashboard/lessons' className={s.menu__list__link} activeClassName={s.is__active}><FiLayers/> Всі уроки</NavLink><
            /li>
          <li className={s.menu__list__item}>
            <NavLink to='/booking' className={s.menu__list__link} activeClassName={s.is__active}><FiStar/> Відгуки</NavLink>
          </li>
        </ul>
      </div>
      <div className={s.menu__items}>
        <span className={s.menu__items__title}>Профіль</span>
        <ul className={s.menu__list}>
          <li className={s.menu__list__item}>
            <NavLink to='/booking' className={s.menu__list__link} activeClassName={s.is__active}><FiUser/> Мій профіль</NavLink>
          </li>
          <li className={s.menu__list__item}>
            <Link to='/' className={s.menu__list__link} onClick={logout}><FiLogOut /> Вихід</Link>
          </li>
        </ul>
      </div>
    </div>
  </div>
}

const mapStateToProps = (state) => ({
  user: state.user
})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logoutUserAction())
})

export default connect(mapStateToProps, mapDispatchToProps)(DashboardNav)