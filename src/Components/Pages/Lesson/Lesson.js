import React, { useEffect, useState } from 'react'

import { useParams } from 'react-router-dom'

import Header from '../../Misc/Header/Header'
import { PageLoader } from '../../Misc/Loader/Loader'
import { fetchLesson } from '../../../store/api/api'
import { LessonCard } from '../../Misc/LessonCard/LessonCard'

export const Lesson = () => {
  const [ lesson, setLesson ] = useState([])
  const [ loading, setLoading ] = useState(true)

  const { id } = useParams()

  useEffect(() => {
    (async () => {
      const { gig } = await fetchLesson(id)

      setLesson(gig)
      setLoading(false)
    })()
  }, [])

  if (loading) return <PageLoader />

  return (<div>
      <Header />
      <div>
        <div>
          <LessonCard lesson={lesson} />
        </div>
      </div>
    </div>
  )
}
