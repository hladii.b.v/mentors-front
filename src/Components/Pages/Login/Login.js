import React, { useEffect, useState } from 'react'

import { withFormik } from 'formik'
import { Redirect, withRouter} from 'react-router'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import s from './Login.module.css'

import Input from '../../Misc/Input/Input'
import { Button } from '../../Misc/Button/Button'
import {
  facebookLoginAction,
  loginAction
} from '../../../store/actions/userActions'
import AuthWrapper from '../../Misc/AuthWrapper/AuthWrapper'
import { PageLoader } from '../../Misc/Loader/Loader'

import { ReactComponent as FiUser } from '../../../assets/user.svg'
import { ReactComponent as FiKey } from '../../../assets/key.svg'

const Login = ({
  values,
  handleChange,
  errors,
  setErrors,
  handleBlur,
  touched,
  handleSubmit
}) => {
  const [ loading, setLoading ] = useState(false)

  useEffect(() => {
    const tempErrors = { ...errors }
    delete tempErrors.password
    setErrors(tempErrors)
  })

  useEffect(() => {
    setLoading(false)
  }, [errors])

  const isValidLogin =
    !errors.password &&
    !errors.username &&
    touched.username &&
    touched.password

  if (localStorage.getItem('mentor_auth_token')) {
    return <Redirect to={{ pathname: '/dashboard' }}/>
  }

  if (loading) return <PageLoader />

  return (
    <AuthWrapper title='Увійдіть'>
      <div className={s.inputs__container}>
        <Input
          containerClass={s.input__container}
          placeholder='john-doe@gmail.com'
          label='E-mail'
          name='username'
          Icon={FiUser}
          isError={errors.username && touched.username}
          onBlur={handleBlur}
          value={values.username}
          onChange={handleChange}
        />
        <Input
          containerClass={s.input__container}
          placeholder='********'
          type='password'
          name='password'
          label={'Пароль'}
          Icon={FiKey}
          iconClass={s.icon}
          isError={errors.password && touched.password}
          onBlur={handleBlur}
          value={values.password}
          onChange={handleChange}
        />
      </div>
      <div className={s.submit__container}>
        <div className={s.submit__row}>
          <Link to='/reset-password'>{'Нагадати пароль'}</Link>
          <Button
            type='submit'
            onClick={() => {
              setLoading(true)
              handleSubmit()
            }}
            isDisabled={!isValidLogin}
            title='Вхід'
            className={s.submit__button}
          />
        </div>
        {
          errors.authentication && <div className={s.submit__error}>
            {errors.authentication}
          </div>
        }
      </div>
    </AuthWrapper>
  )
}

const formikHOC = withFormik({
  mapPropsToValues: () => ({
    username: '',
    password: ''
  }),
  validate: ({ username, password }) => {
    const errors = {}
    const emailRegex = /^(([^<>()[\]\\.,:\s@]+(\.[^<>()[\]\\.,:\s@]+)*)|(.+))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if (!emailRegex.test(username)) {
      errors.email = 'Невірний email'
    }
    if (password.length < 0) {
      errors.password = 'Пароль неможе бути пустим'
    }
    return errors
  },
  handleSubmit: async (
    { username, password },
    { props: { login, history }, setFieldError, setSubmitting }
  ) => {
    const isSuccess = await login({
      username,
      password,
    })

    if (isSuccess) {
      history.push('/dashboard')
    } else {
      setFieldError('authentication', 'При спробі входу сталась помилка, перевірте правильність введених даних')
      setSubmitting(false)
    }
  },
})(Login)

const routerHOC = withRouter(formikHOC)

const mapDispatchToProps = (dispatch) => ({
  login: (data, isRemember) => dispatch(loginAction(data, isRemember)),
  facebookLogin: (data) => dispatch(facebookLoginAction(data)),
})

export default connect(null, mapDispatchToProps)(routerHOC)
