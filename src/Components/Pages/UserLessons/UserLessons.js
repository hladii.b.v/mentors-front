import React, { useEffect, useState } from 'react'

import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Route } from 'react-router'

import s from './UserLessons.module.css'

import { Tag } from '@zendeskgarden/react-tags'
import { Body, Cell, Head, HeaderCell, HeaderRow, Row, Table } from '@zendeskgarden/react-tables'
import { Field, Label, Checkbox } from '@zendeskgarden/react-forms'
import { Dropdown, Item, Menu, Trigger } from '@zendeskgarden/react-dropdowns'
import { Button } from '@zendeskgarden/react-buttons'
import { Close, Footer, FooterItem, Modal, Header as ModalHeader } from '@zendeskgarden/react-modals'

import Header from '../../Misc/Header/Header'
import DashboardNav from '../../Misc/DashboardNav/DashboardNav'
import { deleteLessonsAction, getLessonsAction } from '../../../store/actions/userLessonsActions'
import { PageLoader } from '../../Misc/Loader/Loader'

import { FiMoreVertical } from 'react-icons/fi'

const UserLessons = ({ user, lessons, getLessons, deleteLessons }) => {
  const [ loading, setLoading ] = useState(true)
  const [ selected, setSelected ] = useState([])
  const [ rotated, setRotated ] = useState(true)
  const [ deleting, setDeleting ] = useState([])
  const [ visible, setVisible ] = useState(false)

  useEffect(() => {
    (async () => {
      setLoading(true)

      await getLessons()

      setLoading(false)
    })()
  }, [lessons])

  const isSelected = ({ id }) => {
    return selected.includes(id)
  }

  const isAllSelected = () => {
    return selected.length === lessons.length
  }

  const toggle = (lesson) => {
    const items = [...selected]

    const index = items.findIndex(id => id === lesson.id)

    if (index === -1) {
      items.push(lesson.id)
    } else {
      items.splice(index, 1)
    }

    setSelected(items)
  }

  const toggleAll = () => {
    setSelected( isAllSelected() ? [] : lessons.map(({ id }) => id))
  }

  const closeModal = () => {
    setVisible(false)
    setDeleting([])
  }

  const setDeleted = () => {
    setDeleting(selected)
    setVisible(true)
  }

  const deleteSelected = async () => {
    setVisible(false)

    setLoading(true)
    await deleteLessons({ ids: deleting })
  }

  const overflow = lesson => <Dropdown
    onStateChange={options =>
      Object.prototype.hasOwnProperty.call(options, 'isOpen') && setRotated(options.isOpen)
    }
  >
    <Trigger>
      <Button isBasic>
        <Button.EndIcon isRotated={rotated}>
          <FiMoreVertical />
        </Button.EndIcon>
      </Button>
    </Trigger>
    <Menu>
      <Route>
        {
          ({ history }) => <Item
            value='view'
            onClick={() => history.push(`/lessons/${lesson.id}`)}
          >
            Перегляд
          </Item>
        }
      </Route>
      <Route>
        {
          ({ history }) => <Item
            value='update'
            onClick={() => history.push(`/dashboard/lesson/${lesson.id}`)}
          >
            Редагувати
          </Item>
        }
      </Route>
      <Item
        value='delete'
        onClick={() => {
          setDeleting([lesson.id])
          setVisible(true)
        }}
      >
        Видалити
      </Item>
    </Menu>
  </Dropdown>

  if (loading && !user.id) return <PageLoader />

  return  (
    <>
      <Header />
      <div className={s.container}>
        <DashboardNav />
        <div className={s.board}>
          <div className={s.row}>
            <Button disabled={selected.length === 0} onClick={() => setDeleted()}>Видалити вибранні</Button>
          </div>
          <div className={s.row}>
            <div className={s.col__12}>
              {
                lessons.length === <div>Немає уроків</div>
              }
              <Table style={{ minWidth: 500 }}>
                <Head>
                  <HeaderRow>
                    <HeaderCell>
                      <Field>
                        <Checkbox
                          checked={isAllSelected()}
                          onChange={() => toggleAll()}
                        >
                          <Label hidden>Select</Label>
                        </Checkbox>
                      </Field>
                    </HeaderCell>
                    <HeaderCell>Назва</HeaderCell>
                    <HeaderCell>Статус</HeaderCell>
                    <HeaderCell>Категорія</HeaderCell>
                    <HeaderCell></HeaderCell>
                  </HeaderRow>
                </Head>
                <Body>
                  {
                    lessons.map((item, index) =>
                      <Row key={index}>
                        <Cell>
                          <Field>
                            <Checkbox
                              checked={isSelected(item)}
                              onChange={() => toggle(item)}
                            >
                              <Label hidden>Select</Label>
                            </Checkbox>
                          </Field>
                        </Cell>
                        <Cell>
                          <Link
                            style={{ overflow: 'hidden', textOverflow: 'ellipsis' }}
                            to={`/dashboard/lesson/${item.id}`}
                          >
                            {item.title}
                          </Link>
                        </Cell>
                        <Cell>
                          {
                            item.verified && <Tag hue='green'>Перевірено</Tag>
                          }
                          {
                            !item.verified && <Tag hue='red'>Не перевірено</Tag>
                          }
                        </Cell>
                        <Cell>
                          {
                            item.category
                              ? item.category.title
                              :  <b>&mdash;</b>
                          }
                        </Cell>
                        <Cell>
                          {overflow(item)}
                        </Cell>
                      </Row>
                    )
                  }
                </Body>
              </Table>
            </div>
          </div>
        </div>
      </div>
      {
        visible && (
          <Modal onClose={() => closeModal()}>
            <ModalHeader>Видалити уроки?</ModalHeader>
            <Body>
              <ul>
                {
                  deleting.map((id) => <li>{lessons.find((lesson) => lesson.id === id).title}</li>)
                }
              </ul>
            </Body>
            <Footer>
              <FooterItem>
                <Button onClick={() => closeModal()} isBasic>
                  Скасувати
                </Button>
              </FooterItem>
              <FooterItem>
                <Button isDisabled={deleting.length === 0} isPrimary onClick={() => deleteSelected()}>
                  Видалити
                </Button>
              </FooterItem>
            </Footer>
            <Close aria-label='Close modal' />
          </Modal>
        )
      }
    </>
  )
}

const mapStateToProps = (state) => ({
  user: state.user,
  lessons: state.userLessons.all
})

const mapDispatchToProps = (dispatch) => ({
  getLessons: (data) => dispatch(getLessonsAction(data)),
  deleteLessons: (data) => dispatch(deleteLessonsAction(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(UserLessons)
