import React, { useEffect, useState } from 'react'

import { connect } from 'react-redux'

import s from './Home.module.css'

import Header from '../../Misc/Header/Header'
import { PageLoader } from '../../Misc/Loader/Loader'
import { TemplateCard } from '../../Misc/TemplateCard/TemplateCard'
import { FixedWrapper } from '../../Misc/FixedWrapper/FixedWrapper'
import { CategoryCard } from '../../Misc/CategoryCard/CategoryCard'
import { getLessonsAction } from '../../../store/actions/lesonsActions'
import { getCategoriesAction } from '../../../store/actions/categoriesActions'

const Home = ({ lessons, categories, getLessons, getCategories }) => {
  const [ loading, setLoading ] = useState(true)

  useEffect(() => {
    (async () => {
      await getLessons({ limit: 3 })
      await getCategories({ limit: 6 })

      setLoading(false)
    })()
  }, [])

  if (loading) return <PageLoader />

  return (<>
      <Header showImg={true} />
      {
        categories && <div className={s.section}>
          <h2 className={s.section__title}>Найпопулярніші категорії</h2>
          <FixedWrapper className={s.cards__container}>
            {
              categories.map((category, index) => <CategoryCard key={index} category={category} />)
            }
          </FixedWrapper>
        </div>
      }
      {
        lessons && <div className={s.section}>
          <h2 className={s.section__title}>Топ уроків</h2>
          <FixedWrapper className={s.cards__container}>
            {
              lessons.map((lesson, index) => <TemplateCard key={index} lesson={lesson} />)
            }
          </FixedWrapper>
        </div>
      }
    </>
  )
}

const mapStateToProps = (state) => ({
  lessons: state.lessons.all,
  categories: state.categories.all
})

const mapDispatchToProps = (dispatch) => ({
  getLessons: (data) => dispatch(getLessonsAction(data)),
  getCategories: (data) => dispatch(getCategoriesAction(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
