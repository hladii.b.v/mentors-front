import React, { useEffect, useState } from 'react'

import { useParams } from 'react-router-dom'
import { connect } from 'react-redux'
import classnames from 'classnames'

import s from './CreateLesson.module.css'

import { Dropdown, Field, Menu, Item, Select, Label, Multiselect } from '@zendeskgarden/react-dropdowns'
import { Field as InputField, Label as InputLabel, Input, Textarea, Message } from '@zendeskgarden/react-forms'
import { Tag } from '@zendeskgarden/react-tags'

import { PageLoader } from '../../Misc/Loader/Loader'
import DashboardNav from '../../Misc/DashboardNav/DashboardNav'
import Header from '../../Misc/Header/Header'
import { Durations, Langs, Levels } from '../../../helpers/Lesson'
import { Button } from '../../Misc/Button/Button'
import { LessonValidator } from '../../../helpers/LessonValidator'
import { getCategoriesAction } from '../../../store/actions/categoriesActions'
import { createLessonAction, getLessonsAction, updateLessonAction } from '../../../store/actions/userLessonsActions'
import { useHistory } from 'react-router';

const CreateLesson = ({ user, categories, lessons, getCategories, newLesson, getLessons, updateLesson }) => {
  const [ lesson, setLesson ] = useState({
    title: null,
    category_id: null,
    description: null,
    price: 10,
    exprience_years: 1,
    duration: 1,
    languages: ['ua'],
    level: 'middle'
  })
  const [ loading, setLoading ] = useState(true)
  const [ errors, setErrors ] = useState({})

  const validator = new LessonValidator ()
  const params = useParams()
  const history = useHistory()

  useEffect(() => {
    (async () => {
      await getCategories()
      await getLessons()
    })()
  }, [])

  useEffect(() => {
    fetchLesson()
  }, [lessons])

  useEffect(() => {
    if (Object.keys(errors).length > 0) {
      setErrors(validator.validate(lesson))
    }
  }, [lesson])

  const fetchLesson = () => {
    if (params.hasOwnProperty('id')) {
      const gig = lessons.find(({ id }) => id.toString() === params.id)

      if (gig) {
        setLesson(gig)
        setLoading(false)
      }
    } else {
      setLoading(false)
    }
  }

  const saveLesson = async () => {
    const validate = validator.validate(lesson)

    setErrors(validate)

    if (Object.keys(validate).length === 0) {
      setLoading(true)

      history.push('/dashboard/lessons')

      if (params.hasOwnProperty('id')) {
        await updateLesson(params.id, lesson)
      } else {
        await newLesson(lesson)
      }
    }
  }

  const changeField = (field, value) => {
    setLesson({ ...lesson, [field]: value })
  }

  const getCategory = () => {
    if (lesson.category_id) {
      const category = categories.find(({ id }) => id === lesson.category_id)

      if (category) {
        return category.title
      } else {
        return 'Немає категорії'
      }
    } else {
      return 'Виберіть категорію'
    }
  }

  if (loading || !user.id) return <PageLoader />

  return  (
    <>
      <Header />
      <div className={s.container}>
        <DashboardNav />
        <div className={s.board}>
          <div className={s.row}>
            <div className={classnames(s.col__12)}>
              <InputField className={s.input__container}>
                <InputLabel>Назва</InputLabel>
                <Input
                  validation={errors.hasOwnProperty('title') && 'error'}
                  className={s.create__input}
                  value={lesson.title}
                  onChange={({target}) =>
                    changeField('title', target.value)
                  }
                />
                {
                  errors.hasOwnProperty('title') && <Message validation='error'>{errors.title}</Message>
                }
              </InputField>
            </div>
          </div>
          <div className={s.row}>
            <div className={classnames(s.col__12)}>
              <InputField className={s.input__container}>
                <InputLabel>Опис</InputLabel>
                <Textarea
                  className={classnames(s.create__input, s.textarea__height)}
                  value={lesson.description}
                  onChange={({target}) =>
                    changeField('description', target.value)
                  }
                />
              </InputField>
            </div>
          </div>
          <div className={s.row}>
            <div className={s.col__6}>
              <InputField className={s.input__container}>
                <InputLabel>Ціна, грн/урок</InputLabel>
                <Input
                  type='number'
                  validation={errors.hasOwnProperty('price') && 'error'}
                  step={1}
                  className={s.create__input}
                  value={lesson.price}
                  onChange={({target}) =>
                    changeField('price', parseInt(target.value))
                  }
                />
                {
                  errors.hasOwnProperty('price') && <Message validation='error'>{errors.price}</Message>
                }
              </InputField>
            </div>
            <div className={s.col__6}>
              <InputField className={s.input__container}>
                <InputLabel>Досвід викладання (роки)</InputLabel>
                <Input
                  type='number'
                  className={s.create__input}
                  value={lesson.exprience_years}
                  onChange={({target}) =>
                    changeField('exprience_years', parseInt(target.value))
                  }
                />
              </InputField>
            </div>
          </div>
          <div className={s.row}>
            <div className={s.col__6}>
              <Dropdown
                className={s.create__input}
                selectedItem={lesson.category_id}
                onSelect={(value) => changeField('category_id', value)}
              >
                <Field>
                  <Label>Категорії</Label>
                  <Select className={s.create__input}>
                    {getCategory()}
                  </Select>
                </Field>
                <Menu>
                  {
                    categories.map((category, index) => (
                      <Item key={index} value={category.id}>
                        {category.title}
                      </Item>
                    ))
                  }
                  {
                    categories.length === 0 && <Item disabled>Немає категорій</Item>
                  }
                </Menu>
              </Dropdown>
              {
                errors.hasOwnProperty('category_id') && <Message validation='error'>{errors.category_id}</Message>
              }
            </div>
            <div className={s.col__6}>
              <Dropdown
                className={s.create__input}
                selectedItem={lesson.duration}
                onSelect={(value) => changeField('duration', value)}
              >
                <Field>
                  <Label>Тривалість уроку</Label>
                  <Select className={s.create__input}>
                    {
                      lesson.duration ? ` ${Durations[lesson.duration]} хв` : 'Виберіть тривалість уроку'
                    }
                  </Select>
                </Field>
                <Menu>
                  {
                    Durations.map((item, index) => (
                      <Item key={index} value={index.toString()}>
                        {item} хв
                      </Item>
                    ))
                  }
                </Menu>
              </Dropdown>
              {
                errors.hasOwnProperty('duration') && <Message validation='error'>{errors.duration}</Message>
              }
            </div>
          </div>
          <div className={s.row}>
            <div className={s.col__6}>
              <Dropdown
                className={s.create__input}
                selectedItem={lesson.level}
                onSelect={(value) => changeField('level', value)}
              >
                <Field>
                  <Label>Рівень викладання</Label>
                  <Select className={s.create__input}>
                    {
                      lesson.level ? Levels[lesson.level] : 'Виберіть рівень викладання'
                    }
                  </Select>
                </Field>
                <Menu>
                  {
                    Object.entries(Levels).map(([key, value], index) => (
                      <Item key={index} value={key}>
                        {value}
                      </Item>
                    ))
                  }
                </Menu>
              </Dropdown>
            </div>
            <div className={s.col__6}>
              <Dropdown
                className={s.create__input}
                selectedItems={lesson.languages}
                onSelect={(value) => changeField('languages', value)}
              >
                <Field>
                  <Label>Мова викладання</Label>
                  <Multiselect
                    className={s.create__input}
                    renderItem={({ value, removeValue }) => (
                      <Tag>
                        <span>{Langs[value]}</span>
                        <Tag.Close onClick={() => removeValue()} />
                      </Tag>
                    )}
                  />
                </Field>
                <Menu>
                  {
                    Object.entries(Langs).map(([key, value], index) => (
                      <Item key={index} value={key}>
                        {value}
                      </Item>
                    ))
                  }
                </Menu>
              </Dropdown>
            </div>
          </div>
          <div className={s.row}>
            <div className={s.col__12}>
              <Button className={s.button__border} onClick={() => saveLesson()}>Зберегти</Button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  user: state.user,
  categories: state.categories.all,
  lessons: state.userLessons.all
})

const mapDispatchToProps = (dispatch) => ({
  getCategories: (data) => dispatch(getCategoriesAction(data)),
  newLesson: (data) => dispatch(createLessonAction(data)),
  getLessons: (data) => dispatch(getLessonsAction(data)),
  updateLesson: (id, data) => dispatch(updateLessonAction(id, data))
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateLesson)
