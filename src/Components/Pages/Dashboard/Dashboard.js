import React, { useEffect, useState } from 'react'

import { connect } from 'react-redux'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction'
import moment from 'moment'

import s from './Dashboard.module.css'

import { Modal, Header as ModalHeader, Body, Footer, FooterItem, Close } from '@zendeskgarden/react-modals'
import { Dropdown, Field, Menu, Item, Select, Label } from '@zendeskgarden/react-dropdowns'

import Header from '../../Misc/Header/Header'
import { logoutUserAction } from '../../../store/actions/userActions'
import { createSlotAction, deleteSlotAction, getUserSlotsAction } from '../../../store/actions/userSlotsActions'
import { PageLoader, Loading } from '../../Misc/Loader/Loader'
import { Button } from '../../Misc/Button/Button'
import DashboardNav from '../../Misc/DashboardNav/DashboardNav'

const Dashboard = ({ user, slots, getSlots, createSlot, deleteSlot }) => {
  const [ loading, setLoading ] = useState(true)
  const [ daySlots, setDaySlots ] = useState([])
  const [ visible, setVisible ] = useState(false)
  const [ visibleDelete, setVisibleDelete] = useState(false)
  const [ startAt, setStartAt ] = useState(null)
  const [ endAt, setEndAt ] = useState(null)
  const [ selectedItem, setSelectedItem ] = useState('available')
  const [ deletedId, setDeletedId ] = useState(null)

  useEffect(() => {
    (async () => {
      await getSlots()

      setLoading(false)
    })()
  }, [])

  useEffect(() => {
   if (visible === false) {
     setStartAt(null)
     setEndAt(null)
   }
  }, [visible])

  useEffect(() => {
    if (slots) {
      setDaySlots(
        slots.map(({ id, type, day_of_week, start_at, end_at }) =>
          ({
            id,
            title: getName(type),
            start: `${day_of_week} ${start_at}`,
            end: `${day_of_week} ${end_at}`,
            color: eventsColor[type],
            allow: !ignoredSlots.includes(type)
          })
        )
      )
    }

    setLoading(false)
  }, [slots])

  const getName = (type) => {
    const slot = slotsTypes.find((item) => item.type === type)

    if (slot) {
      return slot.name
    }

    return type
  }

  const saveSlot = async () => {
    setVisible(false)
    setLoading(true)

    await createSlot({ type: selectedItem, from: startAt, to: endAt })
  }

  const deleteSelected = async () => {
    setLoading(true)
    await deleteSlot(deletedId)
    setVisibleDelete(false)
  }

  if (!user.id) return <PageLoader />

  return <>
    <Header />
    <div className={s.container}>
    <DashboardNav />
    <div className={s.board}>
      {
        loading && <Loading />
      }
      {
        !loading && <FullCalendar
          selectable={true}
          plugins={[ timeGridPlugin, dayGridPlugin, interactionPlugin ]}
          initialView='timeGridWeek'
          locale='uk'
          allDaySlot={false}
          slotDuration='00:15:00'
          events={daySlots}
          eventClick={function({ event }) {
            if (!event.allow) {
              return false
            }

            setStartAt(moment(event.start).format('Y-MM-DD HH:mm'))
            setEndAt(moment(event.end).format('Y-MM-DD HH:mm'))
            setVisibleDelete(true)
            setDeletedId(event.id)
          }}
          select={function({ start, end }) {
            setStartAt(moment(start).format('Y-MM-DD HH:mm'))
            setEndAt(moment(end).format('Y-MM-DD HH:mm'))
            setVisible(true)
          }}
          headerToolbar={{
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
          }}
          buttonText={{
            today: 'Сьогодні',
            week: 'Тиждень',
            month: 'Місяць',
            day: 'День'
          }}
        />
      }
    </div>
    {
      visible && (
        <Modal onClose={() => setVisible(false)}>
          <ModalHeader>Створення слоту часу</ModalHeader>
          <Body>
            Початок - {startAt}
            <br />
            Закінчення - {endAt}
            <Dropdown
              selectedItem={selectedItem}
              onSelect={setSelectedItem}
            >
              <Field>
                <Label>Оберіть тип слоту</Label>
                <Select disabled={loading}>{selectedItem ? getName(selectedItem ) : 'Оберіть слот'}</Select>
              </Field>
              <Menu>
                {
                  slotsTypes
                    .filter(({ type }) => !ignoredSlots.includes(type))
                    .map((option, index) => (
                      <Item key={index} value={option.type}>
                        {option.name}
                      </Item>
                    ))
                }
              </Menu>
            </Dropdown>
          </Body>
          <Footer>
            <FooterItem>
              <Button onClick={() => setVisible(false)} isBasic>
                Скасувати
              </Button>
            </FooterItem>
            <FooterItem>
              <Button isDisabled={!selectedItem || selectedItem.length === 0} isPrimary onClick={() => saveSlot()}>
                Зберегти
              </Button>
            </FooterItem>
          </Footer>
          <Close aria-label='Close modal' />
        </Modal>
      )
    }
    {
      visibleDelete && (
        <Modal onClose={() => setVisibleDelete(false)}>
          <ModalHeader>Видалити слоту часу?</ModalHeader>
          <Body>
            Початок - {startAt}
            <br />
            Закінчення - {endAt}
          </Body>
          <Footer>
            <FooterItem>
              <Button onClick={() => setVisibleDelete(false)} isBasic>
                Скасувати
              </Button>
            </FooterItem>
            <FooterItem>
              <Button isDisabled={!deletedId} isPrimary onClick={() => deleteSelected(deletedId)}>
                Видалити
              </Button>
            </FooterItem>
          </Footer>
          <Close aria-label='Close modal' />
        </Modal>
      )
    }
  </div>
  </>
}

const mapStateToProps = (state) => ({
  user: state.user,
  slots: state.userSlots.all
})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logoutUserAction()),
  getSlots: () => dispatch(getUserSlotsAction()),
  createSlot: (data) => dispatch(createSlotAction(data)),
  deleteSlot: (id) => dispatch(deleteSlotAction(id))
})

const ignoredSlots = [
  'waiting',
  'reserved'
]

const slotsTypes = [
  {
    type: 'available',
    name: 'Доступно',
  },
  {
    type: 'blocked',
    name: 'Заблоковано',
  },
  {
    type: 'waiting',
    name: 'Очікується',
  },
  {
    type: 'reserved',
    name: 'Заброньованно',
  }
]

const eventsColor = {
  available: '#A5C8D1',
  blocked: '#D69197',
  waiting: '#B12E42',
  reserved: '#258422'
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
