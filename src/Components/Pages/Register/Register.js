import React, { useEffect, useState } from 'react'

import { withFormik } from 'formik'
import { connect } from 'react-redux'
import { Redirect, withRouter } from 'react-router'
import { useParams } from 'react-router-dom'

import s from './Register.module.css'

import Input from '../../Misc/Input/Input'
import { Button } from '../../Misc/Button/Button'
import {
  facebookLoginAction,
  registerAction,
} from '../../../store/actions/userActions'
import AuthWrapper from '../../Misc/AuthWrapper/AuthWrapper'
import { PageLoader } from '../../Misc/Loader/Loader'

import { ReactComponent as FiUser } from '../../../assets/user.svg'
import { ReactComponent as FiKey } from '../../../assets/key.svg'

const Register = ({
  values,
  handleChange,
  errors,
  setErrors,
  handleBlur,
  touched,
  handleSubmit
}) => {
  const [ loading, setLoading ] = useState(false)

  const { mode } = useParams()

  useEffect(() => {
    if (mode && mode === 'mentor') {
      values.isMentor = true
    }
  }, [mode])

  useEffect(() => {
    const tempErrors = {...errors}
    delete tempErrors.password
    setErrors(tempErrors)
  })

  useEffect(() => {
    setLoading(false)
  }, [errors])

  const isValidLogin =
    !errors.password &&
    !errors.email &&
    touched.email &&
    touched.password &&
    !errors.last_name &&
    !errors.name &&
    touched.name &&
    touched.last_name

  const isValidRegister = !errors.email && touched.email

  if (localStorage.getItem('mentor_auth_token')) {
    return <Redirect to={{ pathname: '/dashboard' }}/>
  }

  if (loading) return <PageLoader />

  return (
    <AuthWrapper title='Зареєструєтеся'>
      <div className={s.inputs__container}>
        <div className={s.inputs__row}>
          <Input
            containerClass={s.input__container}
            placeholder='Ім`я'
            label='Ім`я'
            name='name'
            Icon={FiUser}
            isError={errors.name && touched.name}
            onBlur={handleBlur}
            value={values.name}
            onChange={handleChange}
          />
          <Input
            containerClass={s.input__container}
            placeholder='Прізвище'
            label='Прізвище'
            name='last_name'
            Icon={FiUser}
            isError={errors.last_name && touched.last_name}
            onBlur={handleBlur}
            value={values.last_name}
            onChange={handleChange}
          />
        </div>
        <div className={s.inputs__row}>
          <Input
            containerClass={s.input__container}
            placeholder='john-doe@gmail.com'
            label='E-mail'
            name='email'
            Icon={FiUser}
            isError={errors.email && touched.email}
            onBlur={handleBlur}
            value={values.email}
            onChange={handleChange}
          />
          <Input
            containerClass={s.input__container}
            placeholder='********'
            type='password'
            name='password'
            label={'Пароль'}
            Icon={FiKey}
            iconClass={s.icon}
            isError={errors.password && touched.password}
            onBlur={handleBlur}
            value={values.password}
            onChange={handleChange}
          />
        </div>
      </div>
      <div className={s.submit__container}>
        <div className={s.submit__row}>
          <Button
            type='submit'
            onClick={() => {
              setLoading(true)
              handleSubmit()
            }}
            isDisabled={!isValidLogin && !isValidRegister}
            title={'Реєстарція'}
            className={s.submit__button}
          />
        </div>
        {
          errors.authentication && <div className={s.submit__error}>
            {errors.authentication}
          </div>
        }
      </div>
    </AuthWrapper>
  )
}

const formikHOC = withFormik({
  mapPropsToValues: () => ({
    email: '',
    password: '',
    name: '',
    last_name: ''
  }),
  validate: ({ email, password, name, last_name }) => {
    const errors = {}
    const emailRegex = /^(([^<>()[\]\\.,:\s@]+(\.[^<>()[\]\\.,:\s@]+)*)|(.+))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    if (!emailRegex.test(email)) {
      errors.email = 'Невірний email'
    }
    if (name.length < 1) {
      errors.name = 'Імя не може бути пустим'
    }
    if (last_name.length < 1) {
      errors.last_name = 'Прізвище не може бути пустим'
    }
    if (password.length < 6) {
      errors.password = 'Пароль мусить містити 6 символів'
    }
    return errors
  },
  handleSubmit: async (
    { email, password, name, last_name, isMentor },
    { props: { history, register }, setFieldError, setSubmitting }
  ) => {
    let role = 'student'

    if (isMentor) role = 'mentor'

    const isSuccess = await register({ email, password, name, last_name, role })

    if (isSuccess) {
      history.push('/dashboard')
    } else {
      setFieldError('authentication', 'Можливо ваша пошта вже була використана для реєстрації, спробуйте відновити пароль.')
      setSubmitting(false)
    }
  },
})(Register)

const routerHOC = withRouter(formikHOC)

const mapDispatchToProps = (dispatch) => ({
  register: (data) => dispatch(registerAction(data)),
  facebookLogin: (data) => dispatch(facebookLoginAction(data)),
})

export default connect(null, mapDispatchToProps)(routerHOC)
