import React, { useEffect, useState } from 'react'

import { useParams } from 'react-router-dom'
import { useHistory } from 'react-router'
import { connect } from 'react-redux'

import s from './Lessons.module.css'

import Header from '../../Misc/Header/Header'
import { PageLoader } from '../../Misc/Loader/Loader'
import { TemplateCard } from '../../Misc/TemplateCard/TemplateCard'
import { FixedWrapper } from '../../Misc/FixedWrapper/FixedWrapper'
import { FilterCard } from '../../Misc/FilterCard/FilterCard'
import { getLessonsAction } from '../../../store/actions/lesonsActions'
import { getCategoriesAction } from '../../../store/actions/categoriesActions'

const Lessons = ({ lessons, categories, getLessons, getCategories }) => {
  const [ categoryId, setCategoryId ] = useState(null)
  const [ loading, setLoading ] = useState(true)

  const history = useHistory()
  const { id } = useParams()

  useEffect(() => {
    (async () => {
      await getCategories()
    })()
  }, [])

  useEffect(() => {
    (async () => {
      setLoading(true)

      const params = {
        limit: 9
      }

      let category

      if (categoryId) {
        category = categoryId
      }

      if (id && id !== categoryId) {
        setCategoryId(id)
        category = id
      }

      params.category_id = category

      await getLessons(params)

      setLoading(false)
    })()
  }, [categoryId])

  if (loading) return <PageLoader />

  return (<>
      <Header />
      <div className={s.container}>
        <div className={s.left__section}>
          <FixedWrapper className={s.cards__container}>
            {
              lessons &&  lessons.map((lesson, index) => <TemplateCard key={index} lesson={lesson} />)
            }
            {
              !lessons || lessons.length === 0 && <div className={s.message__container}>
                Немає результатів
              </div>
            }
          </FixedWrapper>
        </div>
        <div className={s.right__section}>
          {
            id && <div className={s.container}>
              <div onClick={() => history.push('../lessons')}>Всі категорії</div>
            </div>
          }
          {
            !id && <>
              <h2 className={s.section__title}>Категорії</h2>
              <FilterCard
                categories={categories}
                fetchCategory={(id) => {
                  if (id !== 'null') {
                    setCategoryId(id)
                  } else {
                    setCategoryId(null)
                  }
                }}
              />
            </>
          }
        </div>
      </div>
    </>
  )
}

const mapStateToProps = (state) => ({
  lessons: state.lessons.all,
  categories: state.categories.all
})

const mapDispatchToProps = (dispatch) => ({
  getLessons: (data) => dispatch(getLessonsAction(data)),
  getCategories: (data) => dispatch(getCategoriesAction(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Lessons)